<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title><?php print $title ?></title>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print drupal_get_html_head(); ?>
  <link rel="stylesheet" href="themes/marvin_2k/layout.css" type="text/css" />
  <link rel="stylesheet" href="themes/marvin_2k/style.css" type="text/css" />
  <link rel="stylesheet" href="themes/marvin_2k/modules.css" type="text/css" />
  <?php if (file_exists('themes/marvin_2k/custom.css')): ?>
    <link rel="stylesheet" href="themes/marvin_2k/custom.css" type="text/css" />
  <?php endif; ?>
</head>

<body <?php print theme("onload_attribute"); ?>>
<div class="pageFrame" id="nodeFrame">
  <div id="header">
    <a href="<?php print url(); ?>" title="Index Page"><img src="themes/marvin_2k/images/logo.png" alt="<?php print $title; ?>" /></a>
<?php if ($display_search): ?>
    <form action="<?php print url("search") ?>" method="post">
      <div id="search">
        <input class="form-text" type="text" size="15" value="" name="keys" /><input class="form-submit" type="submit" value="<?php print("Search")?>" />
      </div>
    </form>
<?php endif; ?>
  </div>
  
<?php if (is_array($links)): ?>
  <ul id="main-nav">
  <?php foreach ($links as $link): ?>
    <li><?php print $link?></li>
  <?php endforeach; ?>
  </ul>
<?php endif; ?>

<?php if ($sidebar_left != ""): ?>
  <div class="sidebar" id="sidebar-left">
    <?php print $sidebar_left ?>
  </div>
<?php endif; ?>

<?php if ($sidebar_right != ""): ?>
  <div class="sidebar" id="sidebar-right">
    <?php print $sidebar_right ?>
  </div>
<?php endif; ?>
  
  <div class="main-content" id="content-<?php print $layout ?>">
<?php if ($breadcrumb != ""): ?>
  <?php print $breadcrumb ?>
<?php endif; ?>
<?php if ($tabs != ""): ?>
  <?php print $tabs ?>
<?php endif; ?>
<?php if ($mission != ""): ?>
  <p id="mission"><?php print $mission ?></p>
<?php endif; ?>
<?php if ($help != ""): ?>
  <p id="help"><?php print $help ?></p>
<?php endif; ?>
<?php if ($message != ""): ?>
  <div id="message"><?php print $message ?></div>
<?php endif; ?>
