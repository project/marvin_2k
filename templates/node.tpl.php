  <div class="node<?php print ($static) ? " static" : ""; ?>">
    <?php if ($page == 0): ?>
      <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php endif; ?>
    
    <div class="info"><?php print t("Posted by") ." ". $name ." on ". $date . $terms ?></div>
    <div class="content">
      <?php print $content ?>
    </div>
<?php if ($links): ?>
    <div class="links"><?php print $links ?></div>
<?php endif; ?>
  </div>
  
