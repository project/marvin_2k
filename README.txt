********************************************************************
                     D R U P A L   THEME                        
********************************************************************
Name: marvin_2k
Version: 1.0
Author: Nicholas Young-Soares
Email: drupal at codemonkeyx dot net
Last update: November 15th, 2003
Drupal: Latest CVS

********************************************************************
DESCRIPTION:

This theme is based on the general layout of the original marvin
theme, with a header, content area, and a sidebar. Everything else
is new, here are some of the features.
 * Written from scratch using the new Drupal theme system.
 
 * Uses CSS, XHTML for all layout. (No tables for layout).
 
 * Multiple block placement options allow for blocks to be displayed
   on the left, right, or both sides of the page.
   
 * Multiple mission statement display options, allow you to display
   the default site mission statement, a custom statement, or no 
   statement at all.
   
 * Optional search box which only appears if a user has access to the
   search functionality.
 
 * Visible page marker used in the main navigation to show which section
   of the site the user is currently viewing.

********************************************************************
SYSTEM REQUIREMENTS:

Drupal: Latest CVS.

********************************************************************
INSTALLATION:

see the INSTALL file in this directory.

********************************************************************
TODO:

- none

********************************************************************
UPCOMING FEATURES:

- none

